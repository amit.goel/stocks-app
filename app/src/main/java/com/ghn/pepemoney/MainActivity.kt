package com.ghn.pepemoney

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import com.ghn.pepemoney.models.database.InstrumentWithQuote
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var mRecipesArrayList = ArrayList<InstrumentWithQuote>()
    private val viewModel: SymbolViewModel by lazy {
        ViewModelProviders.of(this).get(SymbolViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(main_toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        }

        FirebaseMessaging.getInstance().subscribeToTopic("Alerts")
        rvSymbol.layoutManager = LinearLayoutManager(this)
        rvSymbol.adapter = StockAdapter(mRecipesArrayList)
        loadQuotes()
    }

    private fun loadQuotes() {
        viewModel.getQuotes()!!.observe(this, Observer {
            mRecipesArrayList.addAll(it!!)
            rvSymbol.adapter.notifyDataSetChanged()
        })
    }

    class StockAdapter(val data: List<InstrumentWithQuote>) : RecyclerView.Adapter<StockViewHolder>() {
        override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
            holder.bind(data[position])
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding: ViewDataBinding =
                    DataBindingUtil.inflate(layoutInflater, R.layout.stock_row, parent, false)
            return StockViewHolder(binding)
        }

        override fun getItemCount(): Int = data.size
    }

    class StockViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Any) {
            binding.setVariable(BR.data, data)
            binding.executePendingBindings()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            drawer_layout?.openDrawer(GravityCompat.START)
            true
        }
        R.id.mainActionTheme -> {
            Log.d("YOLO", "change clicked")
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

}
