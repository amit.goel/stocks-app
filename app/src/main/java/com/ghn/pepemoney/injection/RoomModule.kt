package com.ghn.pepemoney.injection

import android.content.Context
import com.ghn.pepemoney.models.RoomCurrencyDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RoomModule{
    @Provides
    @Singleton
    fun provideRoomCurrencyDataSource(context: Context) =
            RoomCurrencyDataSource.buildPersistentCurrency(context)
}