package com.ghn.pepemoney.injection

import com.ghn.pepemoney.SymbolViewModel
import com.ghn.pepemoney.injection.module.AppModule
import com.ghn.pepemoney.injection.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (NetworkModule::class), (RoomModule::class)])
interface AppComponent {

    fun inject(symbolViewModel: SymbolViewModel)

}