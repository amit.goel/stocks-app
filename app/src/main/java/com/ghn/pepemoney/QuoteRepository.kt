package com.ghn.pepemoney

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.ghn.pepemoney.models.RemoteCurrencyService
import com.ghn.pepemoney.models.RoomCurrencyDataSource
import com.ghn.pepemoney.models.dao.InstrumentDao
import com.ghn.pepemoney.models.database.Instrument
import com.ghn.pepemoney.models.database.InstrumentWithQuote
import com.ghn.pepemoney.models.database.Quote
import com.ghn.pepemoney.models.remote.Result
import com.ghn.pepemoney.models.remote.TradingQuote
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


interface Repository {
    fun getCurrencyList(): LiveData<List<InstrumentWithQuote>>
}

class QuoteRepository @Inject constructor(private val remoteCurrencyService: RemoteCurrencyService,
                                          private val roomCurrencyDataSource: RoomCurrencyDataSource,
                                          private val context: Context) : Repository {

    private val allCompositeDisposable: MutableList<Disposable> = arrayListOf()
    var singify = 4
    private val compositeDisposable = CompositeDisposable()

    fun getQuotes() {

        val disposable = roomCurrencyDataSource.instrumentDao().getAllCoinsRevenueWithCoin().toObservable()
                .flatMap({ return@flatMap remoteCurrencyService.getQuotes(joinQuotes(it)).toObservable() }
                        , { quote, quotes-> quote to quotes })
                .flatMap({ pair ->
                    val quotes = pair.first.mapIndexed { i, iq -> convertTradingQuote(iq, pair.second.results[i]) }
                    return@flatMap Observable.just(quotes)
                }, { quote, quotes -> quote.first to quotes })

               // .flatMapIterable()
                .map {
                    it.first.mapIndexed { i: Int, br: InstrumentWithQuote ->
                        if (br.coinRevenue != null) {
                            return@mapIndexed Observable.just(roomCurrencyDataSource.quoteDao().update(it.second[i]))
                        } else {
                            return@mapIndexed Observable.just(roomCurrencyDataSource.quoteDao().insert(it.second[i]))
                        }
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = { Log.d("yolo", it.toString()) },
                        onError = { it.printStackTrace() },
                        onComplete = { println("Done!") }
                )
        allCompositeDisposable.add(disposable)
    }

    private fun joinQuotes(it: List<InstrumentWithQuote>): String =
            it.map { ins -> ins.coinDB?.symbol }.joinToString(separator = ",")

    private fun convertTradingQuote(quote: InstrumentWithQuote?, it: TradingQuote): Quote =
            Quote(id = quote?.coinRevenue?.id,
                    askPrice = it.askPrice ?: "0",
                    askSize = it.askSize ?: 0,
                    bidPrice = it.bidPrice ?: "0",
                    bidSize = it.bidSize ?: singify,
                    lastTradePrice = it.lastTradePrice ?: "0",
                    lastExtendedHoursTradePrice = it.lastExtendedHoursTradePrice ?: "0",
                    previousClose = it.previousClose ?: "0",
                    adjustedPreviousClose = it.adjustedPreviousClose ?: "0",
                    previousCloseDate = it.previousCloseDate ?: "0",
                    updatedAt = it.updatedAt ?: "0",
                    instrumentId = quote?.coinDB?.id!!)


    private fun upsert(vh: Pair<Result, List<Instrument>>) {

    }

    private fun insertDefaultSymbols() {

        /*Completable.fromAction { roomCurrencyDataSource.instrumentDao().insertAll(instruments = instruments) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { Log.i(QuoteRepository::class.java.simpleName, "DataSource has been inserted into DB") }*/
    }

    private fun transform(currencies: List<InstrumentWithQuote>): List<InstrumentWithQuote> {
        return currencies.map { it }
    }

    override fun getCurrencyList(): LiveData<List<InstrumentWithQuote>> {
        val roomCurrencyDao = roomCurrencyDataSource.instrumentDao()
        val mutableLiveData = MutableLiveData<List<InstrumentWithQuote>>()
        val disposable = roomCurrencyDao.getAllCoinsRevenueWithCoin().toObservable()
                .filter{!it.isEmpty()}
                .switchIfEmpty(insertDefaults(roomCurrencyDao))
                .debounce(2000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { currencyList -> mutableLiveData.value = transform(currencyList) },
                        { t: Throwable? -> t?.printStackTrace() }
                )
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    private fun insertDefaults(roomCurrencyDao: InstrumentDao): Observable<List<InstrumentWithQuote>>? {
        val instruments: ArrayList<Instrument> = arrayListOf(
                Instrument(null, "AMD"),
                Instrument(null, "MU"),
                Instrument(null, "BLNK"),
                Instrument(null, "AVGR"),
                Instrument(null, "ALB")
        )

        return Observable.fromCallable{ roomCurrencyDataSource.instrumentDao().insertAll(instruments = instruments) }
                .flatMap { roomCurrencyDao.getAllCoinsRevenueWithCoin().toObservable() }
                .subscribeOn(Schedulers.io())
    }
}
