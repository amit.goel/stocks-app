package com.ghn.pepemoney

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.ghn.pepemoney.injection.PepeMoneyApplication
import com.ghn.pepemoney.models.database.InstrumentWithQuote
import javax.inject.Inject

class SymbolViewModel : ViewModel() {

    @Inject
    lateinit var currencyRepository: QuoteRepository

    private var liveCurrencyData: LiveData<List<InstrumentWithQuote>>? = null

    init {
        initializeDagger()
        loadCurrencyList()
    }

    fun getQuotes(): LiveData<List<InstrumentWithQuote>>?{
        return liveCurrencyData
    }

    private fun loadCurrencyList() {
        if (liveCurrencyData == null) {
            liveCurrencyData = MutableLiveData<List<InstrumentWithQuote>>()
            liveCurrencyData = currencyRepository.getCurrencyList()
            currencyRepository.getQuotes()
        }

    }

    private fun initializeDagger() = PepeMoneyApplication.appComponent.inject(this)
}
