package com.ghn.pepemoney.models

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.ghn.pepemoney.models.dao.InstrumentDao
import com.ghn.pepemoney.models.dao.QuoteDao
import com.ghn.pepemoney.models.database.Instrument
import com.ghn.pepemoney.models.database.Quote

@Database(entities = [(Instrument::class), (Quote::class)], version = 1)
abstract class RoomCurrencyDataSource : RoomDatabase() {

    abstract fun instrumentDao(): InstrumentDao
    abstract fun quoteDao(): QuoteDao

    companion object {

        fun buildPersistentCurrency(context: Context): RoomCurrencyDataSource = Room.databaseBuilder(
                context.applicationContext,
                RoomCurrencyDataSource::class.java,
                "pepe.db"
        ).build()
    }
}