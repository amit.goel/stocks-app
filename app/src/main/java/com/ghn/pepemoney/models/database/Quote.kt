package com.ghn.pepemoney.models.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "quote", foreignKeys = [(ForeignKey(entity = Instrument::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("instrumentId"),
        onDelete = CASCADE))])
data class Quote(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "quoteId") var id: Long?,
                      @ColumnInfo(name = "ask_price") var askPrice: String,
                      @ColumnInfo(name = "ask_size") var askSize: Int,
                      @ColumnInfo(name = "bid_price") var bidPrice: String,
                      @ColumnInfo(name = "bid_size") var bidSize: Int,
                      @ColumnInfo(name = "last_trade_price") var lastTradePrice: String,
                      @ColumnInfo(name = "last_extended_hours_trade_price") var lastExtendedHoursTradePrice: String,
                      @ColumnInfo(name = "previous_close") var previousClose: String,
                      @ColumnInfo(name = "adjusted_previous_close") var adjustedPreviousClose: String,
                      @ColumnInfo(name = "previous_close_date") var previousCloseDate: String,
                      @ColumnInfo(name = "updated_at") var updatedAt: String,
                      @ColumnInfo(name = "instrumentId") var instrumentId: Long) {

    fun getPositive(): Boolean = lastTradePrice.toFloat() >= previousClose.toFloat()

    fun getPrice(): String = String.format("%.2f", lastTradePrice.toFloat())

    fun getUpdateDate(): String  {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        val mDate = sdf.parse(updatedAt)
        return SimpleDateFormat("MMMM d, yyyy").format(mDate)
    }

}