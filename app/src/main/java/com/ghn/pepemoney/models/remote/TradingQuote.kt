package com.ghn.pepemoney.models.remote

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

data class Result(
        @JsonProperty("results") var results: List<TradingQuote>
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class TradingQuote(
        @JsonProperty("ask_price") var askPrice: String?,
        @JsonProperty("ask_size") var askSize: Int?,
        @JsonProperty("bid_price") var bidPrice: String?,
        @JsonProperty("bid_Size") var bidSize: Int?,
        @JsonProperty("last_trade_price") var lastTradePrice: String?,
        @JsonProperty("last_extended_hours_trade_price") var lastExtendedHoursTradePrice: String?,
        @JsonProperty("previous_close") var previousClose: String?,
        @JsonProperty("adjusted_previous_close") var adjustedPreviousClose: String?,
        @JsonProperty("previous_close_date") var previousCloseDate: String?,
        @JsonProperty("symbol") var symbol: String?,
        @JsonProperty("updated_at") var updatedAt: String?) {

}