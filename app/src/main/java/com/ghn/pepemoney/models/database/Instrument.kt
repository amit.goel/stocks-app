package com.ghn.pepemoney.models.database

import android.arch.persistence.room.*

@Entity(tableName = "instrument")
data class Instrument(@PrimaryKey(autoGenerate = true) var id: Long?,
                       @ColumnInfo(name = "symbol") var symbol: String)

class InstrumentQuoteJoin {
    @Embedded
    var instrument: Instrument? = null

    @Relation(parentColumn = "id", entityColumn = "instrumentId",entity = Quote::class)
    var quotes: List<Quote?> = ArrayList<Quote>()
}

class InstrumentWithQuote  {
    @Embedded
    var coinDB: Instrument? = null

    @Embedded
    var coinRevenue: Quote? = null
}