package com.ghn.pepemoney.models.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.ghn.pepemoney.models.database.Instrument
import com.ghn.pepemoney.models.database.InstrumentWithQuote
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface InstrumentDao {

    @Query("SELECT * from instrument")
    fun getAll(): Flowable<List<Instrument>>

    /**
     * Inserts specified posts in database.
     * @param posts all the posts to insert in database
     */
    @Insert
    fun insertAll(instruments: List<Instrument>)

    @Query("DELETE from instrument")
    fun deleteAll()


  /*  @Query("SELECT * FROM quote WHERE instrumentId in (:instrumentIds)")
    fun findBySourceId(instrumentIds: List<Long?>): Flowable<List<InstrumentQuoteJoin>>*/

    @Query("select instrument.*, quote.*  from instrument left outer join quote on instrument.id = quote.instrumentId")
    fun getAllCoinsRevenueWithCoin(): Maybe<List<InstrumentWithQuote>>


}