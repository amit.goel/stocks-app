package com.ghn.pepemoney.models

import com.ghn.pepemoney.models.remote.Result
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteCurrencyService {
    @GET("quotes/")
    fun getQuotes(@Query("symbols") symbols: String): Flowable<Result>
}

