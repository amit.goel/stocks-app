package com.ghn.pepemoney

import android.text.TextUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

/**
 * Created by amit on 4/20/18.
 */

class FireBaseMsgService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        when{
            remoteMessage!!.data.isNotEmpty() -> processPayload(remoteMessage.data)
            !TextUtils.isEmpty(remoteMessage.notification?.body) ->
                Timber.d("Message Notification Body: $remoteMessage.notification?.body")
        }
    }

    private fun processPayload(data: Map<String, String>) {
        Timber.d("Message data payload: $data")

        /*if (*//* Check if data needs to be processed by long running job *//* true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }*/
        val message = data["message"]
        Timber.d("onMessageReceived() yolo returned: $message")
        //showNotification(message);
    }

}