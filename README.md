Stock market Android Demo Application

Uses Robinhood API to get stock quotes

Uses DataBinding and LiveData.

Uses Room to cache stock quotes

Network and db calls via RX extensions

Use of Kotlin Language for models and classes

Provide clean UI using constraint layout

TODO:
use Dagger for DI on network and room instances

Add more functionality

Add testing

Add CI configurations.





